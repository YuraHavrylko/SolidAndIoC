﻿using System;
using System.Linq;
using NUnit.Framework;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Presenters;
using SolidAndIoC.UnitTests.PresentersTests;
using SolidAndIoC.UnitTests.ServisesTests.Implementation;

namespace SolidAndIoC.UnitTests.ViewsTests
{
    [TestFixture]
    public class PresenterTest
    {
        static InstitutionAddViewTest _institutionAddViewTest = new InstitutionAddViewTest();
        static InstitutionEditViewTest _institutionEditViewTest = new InstitutionEditViewTest();
        static InstitutionViewTest _institutionViewTest = new InstitutionViewTest();
        static DataStorageTest _dataStorageTests = new DataStorageTest();

        InstitutionsPresenter InstitutionsPresenter = new InstitutionsPresenter(_institutionViewTest, _dataStorageTests,
            new InstitutionsEditPresenterTest(_institutionEditViewTest, _dataStorageTests),
            new InstitutionsAddPresenterTest(_institutionAddViewTest, _dataStorageTests));

        [Test]
        public void AddInstitution_AddNewBar_Succses()
        {
            var institution = new Bar()
            {
                Id = 5,
                Rating = 4,
                Name = "Pizza+",
                isOpened = false,
                Adress = "Tychunu"
            };
            _institutionAddViewTest._institution = institution;
            
            InstitutionsPresenter.AddInstitution();

            Assert.That(_dataStorageTests.GetEntertainmentInstitutions().Any(x => x.Name.Equals(institution.Name)));
        }

        [Test]
        public void AddInstitution_AddNewRestaurant_Error()
        {
            var institution = new Restaurant()
            {
                Adress = "Chentralna st.",
                Id = 4,
                Rating = 3,
                Name = "Urban",
                isOpened = true
            };
            _institutionAddViewTest._institution = institution;

            Assert.That(() => InstitutionsPresenter.AddInstitution(), Throws.TypeOf<Exception>().With.Message.EqualTo("This instutition exist"));
        }

        [Test]
        public void GetEntertainmentInstitutionsById_ReadExistingRestaurant_Succes()
        {
            var id = 2;

            var testInstitutions = _dataStorageTests.GetEntertainmentInstitutionsById(id);
            Assert.That(testInstitutions.Id == id);
        }

        [Test]
        public void GetEntertainmentInstitutionsById_ReadUnExistingRestaurant_Error()
        {
            var id = 10;

            Assert.That(() => _dataStorageTests.GetEntertainmentInstitutionsById(id), Throws.TypeOf<Exception>().With.Message.EqualTo("Object not found"));
        }
        
        [Test]
        public void EditInstitution_EditExistingVBar_Succses()
        {
            var institution = new Restaurant()
            {
                Adress = "Chentralna 44 st.",
                Id = 4,
                Rating = 1,
                Name = "Urban space",
                isOpened = true
            };
            _institutionEditViewTest._institution = institution;

            InstitutionsPresenter.EditInstitution();

            Assert.That(_dataStorageTests.GetEntertainmentInstitutions().Any(x => x.Name.Equals(institution.Name)));
        }

        [Test]
        public void EditingInstitution_EditExistingBar_Error()
        {
            var institution = new Restaurant()
            {
                Adress = "Chentralna 44 st.",
                Id = 4,
                Rating = 1,
                Name = "",
                isOpened = true
            };
            _institutionEditViewTest._institution = institution;

            Assert.That(() => InstitutionsPresenter.EditInstitution(), Throws.TypeOf<Exception>().With.Message.EqualTo("Name is required"));
        }


        

        [Test]
        public void ChangeOpen_OpenOrClose_Succses()
        {

            _institutionViewTest._institution = _dataStorageTests.GetEntertainmentInstitutionsById(1);
            bool isOpened = _dataStorageTests.GetEntertainmentInstitutionsById(1).isOpened;

            InstitutionsPresenter.ChangeOpen();
            Assert.That(_institutionViewTest._institution.isOpened != isOpened);
        }

        [Test]
        public void AddRandom_CreateAndAddRandomBar_Succses()
        {
            InstitutionsPresenter.AddRandom();

            var entertainmentInstitution = _dataStorageTests.GetEntertainmentInstitutions().ToList()[5];

            Assert.That(_dataStorageTests.GetEntertainmentInstitutions().ToList().Any(x => x.Name + "1" == entertainmentInstitution.Name));
        }
        [Test]
        public void RemoteInstitution_RemoteExistingBar_Succses()
        {

            _institutionViewTest._institution = _dataStorageTests.GetEntertainmentInstitutionsById(3);

            InstitutionsPresenter.RemoveInstitution();

            Assert.That(_dataStorageTests.GetEntertainmentInstitutions().ToList().All(x => x.Id != _institutionViewTest._institution.Id));
        }
    }
}