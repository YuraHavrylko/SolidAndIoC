﻿using System;
using System.Linq;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Servises;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.UnitTests.PresentersTests
{
    public class InstitutionsAddPresenterTest : PresenterCustom<IInstitutionAddView, IInstitutionAddPresenter>, IInstitutionAddPresenter
    {
        private IDataStorage _storage;

        public InstitutionsAddPresenterTest(IInstitutionAddView view, IDataStorage storage) : base(view)
        {
            _storage = storage;
        }

        public void SaveInstitution()
        {
            var instutition = View.GetInstitution();
            var institutions = _storage.GetEntertainmentInstitutions();
            if (institutions.Any(x => x.Name.Equals(instutition.Name)))
            {
                throw new Exception("This instutition exist");
            }
            _storage.Add(instutition);
        }

        public void CancelAddingInstitution()
        {
            System.Diagnostics.Debug.WriteLine("Cancel add institution");
        }

        public void PresentInstitution()
        {
            View.SetAddedInstitution();
            System.Diagnostics.Debug.WriteLine("Show add institution");
        }
    }
}