﻿using System;
using System.Linq;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Servises;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.UnitTests.PresentersTests
{
    public class InstitutionsEditPresenterTest : PresenterCustom<IInstitutionEditView, IInstitutionEditPresenter>, IInstitutionEditPresenter
    {
        private IDataStorage _storage;
        public InstitutionsEditPresenterTest(IInstitutionEditView view, IDataStorage storage) : base(view)
        {
            _storage = storage;
        }

        public void PresentInstitution(EntertainmentInstitution institution)
        {
            View.SetEditedInstitution(institution);
            System.Diagnostics.Debug.WriteLine("Show edit institution");
        }

        public void SaveInstitution()
        {
            var instutition = View.GetInstitution();
            if (instutition.Name.Equals("")) 
            {
                throw new Exception("Name is required");
            }
            _storage.Edit(instutition);
        }

        public void CancelEditingInstitution()
        {
            System.Diagnostics.Debug.WriteLine("Cancel edit institution");
        }
    }
}