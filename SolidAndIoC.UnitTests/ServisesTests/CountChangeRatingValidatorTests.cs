﻿using NUnit.Framework;

namespace SolidAndIoC.UnitTests
{
    [TestFixture]
    public class CountChangeRatingValidatorTests
    {
        [Test]
        public void Validate_CheckIfAcceptableRatingChangeIfOpen_ReturnTrue()
        {
            CountChangeRatingValidator validator = new CountChangeRatingValidator();
            validator.NextValidator = new IsCloseValidator() {isClose = true};

            Assert.That(validator.Validate(1, 2));
        }

        [Test]
        public void Validate_CheckIfAcceptableRatingChangeIfClose_ReturnFalse()
        {
            CountChangeRatingValidator validator = new CountChangeRatingValidator();
            validator.NextValidator = new IsCloseValidator() { isClose = false };

            Assert.That(!validator.Validate(1, 2));
        }

        [Test]
        public void Validate_CheckIfAcceptableRatingChangeIfOpenIfChangeMoreTwo_ReturnFalse()
        {
            CountChangeRatingValidator validator = new CountChangeRatingValidator();
            validator.NextValidator = new IsCloseValidator() { isClose = true };

            Assert.That(!validator.Validate(1, 3));
        }
    }
}