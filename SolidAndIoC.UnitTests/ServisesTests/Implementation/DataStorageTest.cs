﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SolidAndIoC.Contracts.Servises;

namespace SolidAndIoC.UnitTests.ServisesTests.Implementation
{
    public class DataStorageTest : IDataStorage
    {
        List<EntertainmentInstitution> _institutions = new List<EntertainmentInstitution>();

        public DataStorageTest()
        {
            _institutions.Add(new Restaurant() {Adress = "Valy st.", Id = 1, Rating = 2, Name = "Franko", isOpened = false});
            _institutions.Add(new Bar() { Adress = "Vovchenecka st.", Id = 2, Rating = 4, Name = "Biblioteka", isOpened = true });
            _institutions.Add(new Bar() { Adress = "Naberezhna st.", Id = 3, Rating = 1, Name = "Pyatnicha", isOpened = false });
            _institutions.Add(new Restaurant() { Adress = "Chentralna st.", Id = 4, Rating = 3, Name = "Urban", isOpened = true });
        }

        public IEnumerable<EntertainmentInstitution> GetEntertainmentInstitutions()
        {
            return _institutions;
        }

        public EntertainmentInstitution GetEntertainmentInstitutionsById(int id)
        {
            var index = _institutions.FindIndex(x => x.Id == id);
            if (index < 0)
            {
                throw new Exception("Object not found");
            }
            return _institutions[index];
        }

        public void Add(EntertainmentInstitution institution)
        {
            institution.Id = _institutions.Select(x => x.Id).Max() + 1;
            _institutions.Add(institution);
        }

        public void AddAll(List<EntertainmentInstitution> institution)
        {
            foreach (var item in institution)
            {
                item.Id = _institutions.Select(x => x.Id).Max() + 1;
                _institutions.Add(item);
            }
        }

        public void Edit(EntertainmentInstitution institution)
        {
            var index = _institutions.FindIndex(x => x.Id == institution.Id);
            if (index < 0)
            {
                throw new Exception("Object not found");
            }
            _institutions[index] = institution;
        }

        public void Remove(EntertainmentInstitution institution)
        {
            var index = _institutions.FindIndex(x => x.Id == institution.Id);
            if (index < 0)
            {
                throw new Exception("Object not found");
            }
            _institutions.Remove(institution);
        }

        public void RemoveById(int id)
        {
            var index = _institutions.FindIndex(x => x.Id == id);
            if (index < 0)
            {
                throw new Exception("Object not found");
            }
            _institutions.Remove(_institutions.FirstOrDefault(x => x.Id == id));
        }
    }
}
