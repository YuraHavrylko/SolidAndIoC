﻿using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Views;

namespace SolidAndIoC.UnitTests.ViewsTests
{
    public class InstitutionAddViewTest : IInstitutionAddView
    {
        public IInstitutionAddPresenter Presenter { get; set; }
        public EntertainmentInstitution _institution;
        public void ShowView()
        {
            
            System.Diagnostics.Debug.WriteLine("Show add view");
        }

        public void CloseView()
        {
            System.Diagnostics.Debug.WriteLine("Close add view");
        }

        public EntertainmentInstitution GetInstitution()
        {
            return _institution;
        }

        public void SetAddedInstitution()
        {
            Presenter.SaveInstitution();
            System.Diagnostics.Debug.WriteLine("Set added institutions");
        }
    }
}