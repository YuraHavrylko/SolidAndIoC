﻿using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Views;

namespace SolidAndIoC.UnitTests.ViewsTests
{
    public class InstitutionEditViewTest : IInstitutionEditView
    {
        public IInstitutionEditPresenter Presenter { get; set; }
        public EntertainmentInstitution _institution;
        public void ShowView()
        {
            System.Diagnostics.Debug.WriteLine("Show edit view");
        }

        public void CloseView()
        {
            System.Diagnostics.Debug.WriteLine("Close edit view");
        }

        public void SetEditedInstitution(EntertainmentInstitution institution)
        {
            Presenter.SaveInstitution();
            System.Diagnostics.Debug.WriteLine("Set edited institutions");
        }

        public EntertainmentInstitution GetInstitution()
        {
            return _institution;
        }
    }
}