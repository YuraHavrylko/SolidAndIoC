﻿using System.Collections.Generic;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Views;

namespace SolidAndIoC.UnitTests.ViewsTests
{
    public class InstitutionViewTest : IInstitutionView
    {
        public IInstitutionPresenter Presenter { get; set; }
        public EntertainmentInstitution _institution;
        public void ShowView()
        {
            System.Diagnostics.Debug.WriteLine("Show view");
        }

        public void CloseView()
        {
            System.Diagnostics.Debug.WriteLine("Close view");
        }

        public void Refresh(IEnumerable<EntertainmentInstitution> institutions)
        {
            System.Diagnostics.Debug.WriteLine("Refresh list");
        }

        public EntertainmentInstitution GetSelectedInstitution()
        {
            return _institution;
        }
    }
}