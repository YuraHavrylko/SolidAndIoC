﻿using System.Configuration;

namespace SolidAndIoC
{
    public class AppSettings
    {
        private static AppSettings _instance;

        private AppSettings()
        {
        }

        public static AppSettings Instance
        {
            get { return _instance ?? (_instance = new AppSettings()); }
        }

        public string InstitutionFilePath
        {
            get { return ConfigurationManager.AppSettings["institutionsFilePath"]; }
        }
    }
}