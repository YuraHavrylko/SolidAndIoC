﻿namespace SolidAndIoC.Contracts.Presenters
{
    public interface IInstitutionAddPresenter
    {
        void SaveInstitution();
        void CancelAddingInstitution();
        void PresentInstitution();
    }
}