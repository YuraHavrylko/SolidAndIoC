﻿namespace SolidAndIoC.Contracts.Presenters
{
    public interface IInstitutionEditPresenter
    {
        void PresentInstitution(EntertainmentInstitution institution);
        void SaveInstitution();
        void CancelEditingInstitution();
    }
}