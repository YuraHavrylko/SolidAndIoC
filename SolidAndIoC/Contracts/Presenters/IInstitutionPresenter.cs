﻿namespace SolidAndIoC.Contracts.Presenters
{
    public interface IInstitutionPresenter
    {
        void AddInstitution();
        void EditInstitution();
        void RemoveInstitution();

        void Refresh();
        void AddRandom();
        void RecalculateRating();
        void ChangeOpen();
    }
}