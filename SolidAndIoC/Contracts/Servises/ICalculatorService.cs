﻿using System.Collections.Generic;

namespace SolidAndIoC.Contracts.Servises
{
    public interface ICalculatorService
    {
        List<EntertainmentInstitution> Calculate(List<EntertainmentInstitution> institutions, int count);
        EntertainmentInstitution CloneRandom(List<EntertainmentInstitution> institutions);
        List<EntertainmentInstitution> SetRandomRating(List<EntertainmentInstitution> institutions);
        EntertainmentInstitution OpenCloseInstitution(EntertainmentInstitution institution);
    }
}