﻿using System.Collections.Generic;

namespace SolidAndIoC.Contracts.Servises
{
    public interface IDataStorage
    {
        IEnumerable<EntertainmentInstitution> GetEntertainmentInstitutions();

        EntertainmentInstitution GetEntertainmentInstitutionsById(int id);

        void Add(EntertainmentInstitution institution);
        void AddAll(List<EntertainmentInstitution> institution);
        void Edit(EntertainmentInstitution institution);
        void Remove(EntertainmentInstitution institution);

        void RemoveById(int id);
    }
}