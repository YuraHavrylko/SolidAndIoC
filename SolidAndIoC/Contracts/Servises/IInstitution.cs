﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    interface IInstitution
    {
        void Open();
        void Close();
    }
}
