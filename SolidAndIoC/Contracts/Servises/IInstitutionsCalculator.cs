﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    public interface IInstitutionsCalculator<TInstitution>
    {
        IEnumerable<TInstitution> GetRatingInstitutions(IEnumerable<TInstitution> institutions, int count);
        TInstitution GetRandomInstitution(IEnumerable<TInstitution> institutions);
        TInstitution CloseOpen(TInstitution institution);
    }
}
