﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    interface IObservable
    {
        void RegisterObserver(IObserver obj);
        void RemoveObserver(IObserver obj);
        void NotifyObservers();
    }
}
