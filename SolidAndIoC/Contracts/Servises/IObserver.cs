﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    public interface IObserver
    {
        string UpdateRating(Object obj);
    }
}
