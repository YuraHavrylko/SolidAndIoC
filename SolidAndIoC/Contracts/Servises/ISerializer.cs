﻿
namespace SolidAndIoC.Contracts.Servises
{
    public interface ISerializer
    {
        string Serialize<T>(T itemForSerialize) where T : class;
        T Deserialize<T>(string path) where T : class;
    }
}