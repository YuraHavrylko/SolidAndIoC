﻿using SolidAndIoC.Infrastrucutre;
using SolidAndIoC.Contracts.Presenters;

namespace SolidAndIoC.Contracts.Views
{
    public interface IInstitutionAddView : IView<IInstitutionAddPresenter>
    {
        EntertainmentInstitution GetInstitution();
        void SetAddedInstitution();
    }
}