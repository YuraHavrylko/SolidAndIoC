﻿using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.Contracts.Views
{
    public interface IInstitutionEditView : IView<IInstitutionEditPresenter>
    {
        void SetEditedInstitution(EntertainmentInstitution institution);
        EntertainmentInstitution GetInstitution();
    }
}