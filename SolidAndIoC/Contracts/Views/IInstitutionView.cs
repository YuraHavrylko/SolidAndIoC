﻿using System.Collections.Generic;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.Contracts.Views
{
    public interface IInstitutionView : IView<IInstitutionPresenter>
    {
        void Refresh(IEnumerable<EntertainmentInstitution> institutions);
        EntertainmentInstitution GetSelectedInstitution();
    }
}