﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    [DataContract]
    public class Bar : EntertainmentInstitution
    {
        public Bar()
        {
            observers = new List<IObserver>();
        }
        [DataMember]
        public override int Rating
        {
            get { return this.m_rating; }
            set
            {
                IsCloseValidator closeValidator = new IsCloseValidator() { isClose = isOpened };
                CountChangeRatingValidator changeRatingValidator = new CountChangeRatingValidator() { NextValidator = closeValidator };
                if (changeRatingValidator.Validate(m_rating, value) || m_rating == 0)
                {
                    if (m_rating != 0)
                    {
                        NotifyObservers();
                    }
                    
                    if ((value <= 5) && (value >= 1))
                    {
                        this.m_rating = value;
                    }
                    else
                    {
                        throw new ArgumentException(String.Format("Wrong Bar {0} rating({1}).", Name, value));
                    }
                }
                else
                {
                     throw new ArgumentException(String.Format("Can not change Bar {0} rating to {1}", Name, value));
                }
            }
        }

        public override object Clone()
        {
            return new Bar()
            {
                Rating = this.Rating,
                Id = this.Id,
                isOpened = this.isOpened,
                Adress = this.Adress,
                Name = this.Name
            };
        }

        public override void SetRandomRating()
        {
            Rating = new Random().Next(1, 6);
        }
    }
}
