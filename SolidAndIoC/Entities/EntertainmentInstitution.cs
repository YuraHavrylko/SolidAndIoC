﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    [DataContract]
    [KnownType(typeof(Restaurant))]
    [KnownType(typeof(Bar))]
    public abstract class EntertainmentInstitution : IInstitution, ICloneable, IObservable
    {
        public EntertainmentInstitution()
        {
            
        }

        protected static List<IObserver> observers;
        protected int m_rating = 0;
        [DataMember]
        public int Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Adress { set; get; }
        [DataMember]
        public bool isOpened { set; get; }
        [DataMember]
        public abstract int Rating { set; get; }

        public void Open()
        {
            isOpened = true;
        }

        public void Close()
        {
            isOpened = false;
        }

        public abstract object Clone();

        public abstract void SetRandomRating();


        public void RegisterObserver(IObserver obj)
        {
            observers.Add(obj);
        }

        public void RemoveObserver(IObserver obj)
        {
            observers.Remove(obj);
        }

        public void NotifyObservers()
        {
            foreach (IObserver obj in observers)
            {
                obj.UpdateRating(this);
            }
        }

#pragma warning disable CS0114 // 'EntertainmentInstitution.ToString()' hides inherited member 'object.ToString()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        public string ToString()
#pragma warning restore CS0114 // 'EntertainmentInstitution.ToString()' hides inherited member 'object.ToString()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword.
        {
            return String.Format("{0}\t{1}\t{2}\t{3}\t{4}", Id, Name, Adress, isOpened, Rating);
        }

    }
}
