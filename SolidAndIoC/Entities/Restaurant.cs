﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    [DataContract]
    public class Restaurant : EntertainmentInstitution
    {
        public Restaurant()
        {
            observers = new List<IObserver>();
        }
        [DataMember]
        public override int Rating
        {
            get { return this.m_rating; }
            set
            {
                IsCloseValidator closeValidator = new IsCloseValidator() {isClose = isOpened};
                CountChangeRatingValidator changeRatingValidator = new CountChangeRatingValidator() {NextValidator = closeValidator};
                if (changeRatingValidator.Validate(m_rating, value) || m_rating == 0)
                {
                    if (m_rating != 0)
                    {
                        NotifyObservers();
                    }
                    if ((value <= 3) && (value >= 1))
                    {
                        this.m_rating = value;
                    }
                    else
                    {
                        throw new ArgumentException(String.Format("Wrong Restaurant {0} rating({1}).", Name, value));
                    }
                }
                else
                {
                    throw new ArgumentException(String.Format("Can not change Restaurant {0} rating to {1}", Name, value));
                }    
            }
        }

        public override object Clone()
        {
            return new Restaurant()
            {
                Rating = this.Rating,
                Id = this.Id,
                isOpened = this.isOpened,
                Adress = this.Adress,
                Name = this.Name
            };
        }

        public override void SetRandomRating()
        {
            Rating = new Random().Next(1, 4);
  
        }
    }
}
