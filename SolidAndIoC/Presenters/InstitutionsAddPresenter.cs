﻿using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Servises;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.Presenters
{
    public class InstitutionsAddPresenter : PresenterCustom<IInstitutionAddView, IInstitutionAddPresenter>, IInstitutionAddPresenter
    {
        private IDataStorage _storage;

        public InstitutionsAddPresenter(IInstitutionAddView view, IDataStorage storage) : base(view)
        {
            _storage = storage;
        }

        public void SaveInstitution()
        {
            var institution = View.GetInstitution();
            _storage.Add(institution);
            View.CloseView();
        }

        public void CancelAddingInstitution()
        {
            View.CloseView();
        }

        public void PresentInstitution()
        {
            View.SetAddedInstitution();
            View.ShowView();
        }
    }
}