﻿using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Servises;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.Presenters
{
    public class InstitutionsEditPresenter : PresenterCustom<IInstitutionEditView, IInstitutionEditPresenter>, IInstitutionEditPresenter
    {
        private IDataStorage _storage;
        private EntertainmentInstitution _editInstitution;

        public InstitutionsEditPresenter(IInstitutionEditView view, IDataStorage storage) : base(view)
        {
            _storage = storage;
        }

        public void PresentInstitution(EntertainmentInstitution institution)
        {
            _editInstitution = institution;
            View.SetEditedInstitution(institution);
            View.ShowView();
        }

        public void SaveInstitution()
        {
            var institution = View.GetInstitution();
            institution.Id = _editInstitution.Id;
            _storage.Edit(institution);
            View.CloseView();
        }

        public void CancelEditingInstitution()
        {
            View.CloseView();
        }
    }
}