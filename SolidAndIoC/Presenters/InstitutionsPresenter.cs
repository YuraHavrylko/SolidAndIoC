﻿using System;
using System.Linq;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Servises;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Infrastrucutre;

namespace SolidAndIoC.Presenters
{
    public class InstitutionsPresenter : PresenterCustom<IInstitutionView, IInstitutionPresenter>, IInstitutionPresenter
    {

        private IDataStorage _providerStorage;
        private IInstitutionEditPresenter _institutionEditPresenter;
        private IInstitutionAddPresenter _institutionAddPresenter;
        private ICalculatorService _calculatorServise;
        public InstitutionsPresenter(IInstitutionView view, IDataStorage providerStorage,
            IInstitutionEditPresenter institutionEditPresenter, IInstitutionAddPresenter institutionAddPresenter) : base(view)
        {
            _calculatorServise = new CalculatorServise(new Calculator<EntertainmentInstitution>());
            _providerStorage = providerStorage;
            _institutionEditPresenter = institutionEditPresenter;
            _institutionAddPresenter = institutionAddPresenter;
            ReInitialize();
        }

        public void AddInstitution()
        {
            _institutionAddPresenter.PresentInstitution();
            ReInitialize();
        }

        public void EditInstitution()
        {
            var institution = View.GetSelectedInstitution();
            _institutionEditPresenter.PresentInstitution(institution);
            ReInitialize();
        }

        public void RemoveInstitution()
        {
            var removeObject = View.GetSelectedInstitution();
            _providerStorage.RemoveById(removeObject.Id);
            ReInitialize();
        }

        public void Refresh()
        {
            ReInitialize();
        }

        public void AddRandom()
        {
            EntertainmentInstitution institution =
                _calculatorServise.CloneRandom(_providerStorage.GetEntertainmentInstitutions().ToList());
            _providerStorage.Add(institution);
            ReInitialize();
        }

        public void RecalculateRating()
        {
            _providerStorage.AddAll(_calculatorServise.SetRandomRating(_providerStorage.GetEntertainmentInstitutions().ToList()));
            ReInitialize();
        }

        public void ChangeOpen()
        {
            var closeOpenObj = View.GetSelectedInstitution();
            closeOpenObj = _providerStorage.GetEntertainmentInstitutionsById(closeOpenObj.Id);
            _providerStorage.Remove(closeOpenObj);
            closeOpenObj = _calculatorServise.OpenCloseInstitution(closeOpenObj);
            _providerStorage.Add(closeOpenObj);
            ReInitialize();
        }

        private void ReInitialize()
        {
            var institution = _providerStorage.GetEntertainmentInstitutions();
            View.Refresh(institution);
        }
    }
}