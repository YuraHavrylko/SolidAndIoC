﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Infrastrucutre;
using Timer = System.Timers.Timer;


namespace SolidAndIoC
{

    class Program
    {
        [STAThreadAttribute]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var presenter = ServiceLocator.Instance.Resolve<IInstitutionPresenter>();
            var institutionPresenter = presenter as PresenterCustom<IInstitutionView, IInstitutionPresenter>;
            var view = institutionPresenter.View as Form;

            Application.Run(view);
        }
    }
}
