﻿//using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Servises;
using SolidAndIoC.Contracts.Views;
using SolidAndIoC.Presenters;
using SolidAndIoC.Views;

namespace SolidAndIoC
{
    public class ServiceLocator
    {
        private IUnityContainer _container;
        private static ServiceLocator _servsceLocator;
        public static ServiceLocator Instance
        {
            get
            {
                return _servsceLocator ?? (_servsceLocator = new ServiceLocator());
            }
        }

        private ServiceLocator()
        {
            _container = new UnityContainer();

            InitiaizeServices(_container);
            InitializeViews(_container);
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        private void InitiaizeServices(IUnityContainer container)
        {
            container.RegisterType<ISerializer, Serializer>(new ContainerControlledLifetimeManager());
            container.RegisterType<IDataStorage, DataStorage>();
        }

        private void InitializeViews(IUnityContainer container)
        {
            container.RegisterType<IInstitutionPresenter, InstitutionsPresenter>();
            container.RegisterType<IInstitutionAddPresenter, InstitutionsAddPresenter>();
            container.RegisterType<IInstitutionEditPresenter, InstitutionsEditPresenter>();

            container.RegisterType<IInstitutionView, InstitutionView>();
            container.RegisterType<IInstitutionEditView, InstitutionEditView>();
            container.RegisterType<IInstitutionAddView, InstitutionAddView>();
        }
    }
}