﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    public class Calculator<TInstitution> : IInstitutionsCalculator<TInstitution>
        where TInstitution : EntertainmentInstitution 
    {
        
        public IEnumerable<TInstitution>  GetRatingInstitutions(IEnumerable<TInstitution> institutions, int count)
        {
            var item = institutions.OfType<TInstitution>().OrderByDescending(n => n.Rating).Take(count).ToList();
            return new List<TInstitution>(item);
        }

        public TInstitution GetRandomInstitution(IEnumerable<TInstitution> institutions)
        {
            var item = institutions.OfType<TInstitution>().OrderBy(a => Guid.NewGuid()).First();
            return item;
        }

        public TInstitution CloseOpen(TInstitution institution)
        {
            if (institution.isOpened)
            {
                institution.Close();
            }
            else
            {
                institution.Open();
            }
            return institution;
        }
    }
}
