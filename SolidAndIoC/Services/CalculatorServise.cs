﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Ninject.Infrastructure.Language;
using SolidAndIoC.Contracts.Servises;

namespace SolidAndIoC
{
    public class CalculatorServise : ICalculatorService
    {
        private readonly IInstitutionsCalculator<EntertainmentInstitution> _calculator;

        public CalculatorServise(IInstitutionsCalculator<EntertainmentInstitution> calculator)
        {
            _calculator = calculator;
        }

        public List<EntertainmentInstitution> Calculate(List<EntertainmentInstitution> institutions, int count)
        {
            return _calculator.GetRatingInstitutions(institutions, count).ToList();
        }

        public EntertainmentInstitution CloneRandom(List<EntertainmentInstitution> institutions)
        {
            var random = _calculator.GetRandomInstitution(institutions);
            var clone = random.Clone() as EntertainmentInstitution;
            clone.Name = clone.Name + "1";
            return clone;
        }

        public List<EntertainmentInstitution> SetRandomRating(List<EntertainmentInstitution> institutions)
        {
            var item = _calculator.GetRandomInstitution(institutions);
            item.SetRandomRating();
            return institutions;
        }

        public EntertainmentInstitution OpenCloseInstitution(EntertainmentInstitution institution)
        {
            var item = _calculator.CloseOpen(institution);
            return item;
        }
    }
}
