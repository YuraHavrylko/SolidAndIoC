﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    public class CountChangeRatingValidator : IRatingValidator
    {
        public IRatingValidator NextValidator { get; set; }
        public bool Validate(int oldRating, int newRating)
        {
            var isValid = Math.Abs(oldRating - newRating) <= 1;
            return isValid && NextValidator.Validate(oldRating, newRating);
        }
    }
}
