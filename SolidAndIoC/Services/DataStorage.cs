﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using SolidAndIoC.Contracts.Servises;

namespace SolidAndIoC
{
    [DataContract]
    [KnownType(typeof(Restaurant))]
    [KnownType(typeof(Bar))]
    public class DataStorage : IDataStorage
    {

        private ISerializer _serializer;
        private readonly string _path;

        public DataStorage(ISerializer serializer)
        {
            _path = "D:\\Projects\\C++\\VStudioProjectsC++\\Module2C#\\SolidAndIoC\\SolidAndIoC\\Base.xml";
            _serializer = serializer;
            Initialize();
        }

        public IEnumerable<EntertainmentInstitution> GetEntertainmentInstitutions()
        {
            var institutions = _serializer.Deserialize<IEnumerable<EntertainmentInstitution>>(_path);
            return institutions;
        }

        public EntertainmentInstitution GetEntertainmentInstitutionsById(int id)
        {
            var institutions = GetEntertainmentInstitutions();
            var institution = institutions.FirstOrDefault(x => x.Id == id);
            return institution;
        }
        
        public void Add(EntertainmentInstitution institution)
        {
            var institutions = GetEntertainmentInstitutions().ToList();
            institutions.Add(institution);
            SaveChanges(institutions);
        }

        public void AddAll(List<EntertainmentInstitution> institution)
        {
            var institutions = institution;
            SaveChanges(institutions);
        }

        public void Edit(EntertainmentInstitution institution)
        {
            var institutions = GetEntertainmentInstitutions().ToList();
            var removeObject = institutions.FirstOrDefault(t => t.Id == institution.Id);
            if (removeObject == null)
            {
                return;
            }
            institutions.Remove(removeObject);
            institutions.Add(institution);
            SaveChanges(institutions);
        }

        public void Remove(EntertainmentInstitution institution)
        {
            var institutions = GetEntertainmentInstitutions().ToList();
            var removeObject = institutions.FirstOrDefault(t => t.Id == institution.Id);
            if (removeObject == null)
            {
                return;
            }
            institutions.Remove(removeObject);
            SaveChanges(institutions);
        }

        public void RemoveById(int id)
        {
            var institutions = GetEntertainmentInstitutions().ToList();
            var removeObject = institutions.FirstOrDefault(t => t.Id == id);
            if (removeObject == null)
            {
                return;
            }
            institutions.Remove(removeObject);
            SaveChanges(institutions);
        }


        private void Initialize()
        {
            if (File.Exists(_path))
            {
                return;
            }
            var institutions = new List<EntertainmentInstitution>();
            SaveChanges(institutions);
        }

        private void SaveChanges(IEnumerable<EntertainmentInstitution> institutions)
        {
            var serialized = _serializer.Serialize<List<EntertainmentInstitution>>(institutions.ToList());
            File.WriteAllText(_path, serialized);
        }
    }
}