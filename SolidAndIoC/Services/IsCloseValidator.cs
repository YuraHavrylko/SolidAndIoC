﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidAndIoC
{
    public class IsCloseValidator : IRatingValidator
    {
        public IRatingValidator NextValidator { get; set; }
        public bool isClose { get; set; }
        public bool Validate(int oldRating, int newRating)
        {
            return isClose;
        }
    }

}
