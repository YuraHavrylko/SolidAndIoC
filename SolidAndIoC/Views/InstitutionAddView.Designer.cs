﻿namespace SolidAndIoC.Views
{
    partial class InstitutionAddView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.frpBoxEntertaimentInstitution = new System.Windows.Forms.GroupBox();
            this.rdioRestaurant = new System.Windows.Forms.RadioButton();
            this.rdioBar = new System.Windows.Forms.RadioButton();
            this.edtId = new System.Windows.Forms.NumericUpDown();
            this.editRating = new System.Windows.Forms.NumericUpDown();
            this.btnCancel = new System.Windows.Forms.Button();
            this.brnSave = new System.Windows.Forms.Button();
            this.edtAdress = new System.Windows.Forms.TextBox();
            this.edtName = new System.Windows.Forms.TextBox();
            this.checkOpen = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.frpBoxEntertaimentInstitution.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editRating)).BeginInit();
            this.SuspendLayout();
            // 
            // frpBoxEntertaimentInstitution
            // 
            this.frpBoxEntertaimentInstitution.Controls.Add(this.rdioRestaurant);
            this.frpBoxEntertaimentInstitution.Controls.Add(this.rdioBar);
            this.frpBoxEntertaimentInstitution.Location = new System.Drawing.Point(79, 22);
            this.frpBoxEntertaimentInstitution.Name = "frpBoxEntertaimentInstitution";
            this.frpBoxEntertaimentInstitution.Size = new System.Drawing.Size(134, 60);
            this.frpBoxEntertaimentInstitution.TabIndex = 27;
            this.frpBoxEntertaimentInstitution.TabStop = false;
            this.frpBoxEntertaimentInstitution.Text = "Entertaiment Institutions";
            // 
            // rdioRestaurant
            // 
            this.rdioRestaurant.AutoSize = true;
            this.rdioRestaurant.Location = new System.Drawing.Point(11, 19);
            this.rdioRestaurant.Name = "rdioRestaurant";
            this.rdioRestaurant.Size = new System.Drawing.Size(77, 17);
            this.rdioRestaurant.TabIndex = 13;
            this.rdioRestaurant.TabStop = true;
            this.rdioRestaurant.Text = "Restaurant";
            this.rdioRestaurant.UseVisualStyleBackColor = true;
            // 
            // rdioBar
            // 
            this.rdioBar.AutoSize = true;
            this.rdioBar.Location = new System.Drawing.Point(11, 35);
            this.rdioBar.Name = "rdioBar";
            this.rdioBar.Size = new System.Drawing.Size(41, 17);
            this.rdioBar.TabIndex = 14;
            this.rdioBar.TabStop = true;
            this.rdioBar.Text = "Bar";
            this.rdioBar.UseVisualStyleBackColor = true;
            // 
            // edtId
            // 
            this.edtId.Location = new System.Drawing.Point(36, 22);
            this.edtId.Name = "edtId";
            this.edtId.Size = new System.Drawing.Size(36, 20);
            this.edtId.TabIndex = 26;
            // 
            // editRating
            // 
            this.editRating.Location = new System.Drawing.Point(79, 169);
            this.editRating.Name = "editRating";
            this.editRating.Size = new System.Drawing.Size(37, 20);
            this.editRating.TabIndex = 25;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(126, 215);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 24;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // brnSave
            // 
            this.brnSave.Location = new System.Drawing.Point(27, 215);
            this.brnSave.Name = "brnSave";
            this.brnSave.Size = new System.Drawing.Size(75, 23);
            this.brnSave.TabIndex = 23;
            this.brnSave.Text = "Save";
            this.brnSave.UseVisualStyleBackColor = true;
            this.brnSave.Click += new System.EventHandler(this.brnSave_Click);
            // 
            // edtAdress
            // 
            this.edtAdress.Location = new System.Drawing.Point(79, 130);
            this.edtAdress.Name = "edtAdress";
            this.edtAdress.Size = new System.Drawing.Size(100, 20);
            this.edtAdress.TabIndex = 22;
            // 
            // edtName
            // 
            this.edtName.Location = new System.Drawing.Point(79, 89);
            this.edtName.Name = "edtName";
            this.edtName.Size = new System.Drawing.Size(102, 20);
            this.edtName.TabIndex = 21;
            // 
            // checkOpen
            // 
            this.checkOpen.AutoSize = true;
            this.checkOpen.Location = new System.Drawing.Point(138, 172);
            this.checkOpen.Name = "checkOpen";
            this.checkOpen.Size = new System.Drawing.Size(63, 17);
            this.checkOpen.TabIndex = 20;
            this.checkOpen.Text = "Is Open";
            this.checkOpen.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 173);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Rating";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Adress";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Name";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "ID";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(12, 260);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 28;
            // 
            // InstitutionAddView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 282);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.frpBoxEntertaimentInstitution);
            this.Controls.Add(this.edtId);
            this.Controls.Add(this.editRating);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.brnSave);
            this.Controls.Add(this.edtAdress);
            this.Controls.Add(this.edtName);
            this.Controls.Add(this.checkOpen);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InstitutionAddView";
            this.Text = "InstitutionAddView";
            this.frpBoxEntertaimentInstitution.ResumeLayout(false);
            this.frpBoxEntertaimentInstitution.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editRating)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox frpBoxEntertaimentInstitution;
        private System.Windows.Forms.RadioButton rdioRestaurant;
        private System.Windows.Forms.RadioButton rdioBar;
        private System.Windows.Forms.NumericUpDown edtId;
        private System.Windows.Forms.NumericUpDown editRating;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button brnSave;
        private System.Windows.Forms.TextBox edtAdress;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.CheckBox checkOpen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblError;
    }
}