﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Views;

namespace SolidAndIoC.Views
{
    public partial class InstitutionAddView : Form, IInstitutionAddView
    {
        public InstitutionAddView()
        {
            InitializeComponent();
        }

        public IInstitutionAddPresenter Presenter { get; set; }
        public void ShowView()
        {
            ShowDialog();
        }

        public void CloseView()
        {
            Close();
        }

        public void SetAddedInstitution()
        {
            rdioRestaurant.Checked = true;
            rdioBar.Checked = false;
            edtId.Text = "";
            edtName.Text = "";
            edtAdress.Text = "";
            editRating.Text = "";
            checkOpen.Checked = false;
        }

        public EntertainmentInstitution GetInstitution()
        {
            if (rdioRestaurant.Checked)
            {
                return new Restaurant()
                {
                    Id = Convert.ToInt32(edtId.Value),
                    Name = edtName.Text,
                    Adress = edtAdress.Text,
                    isOpened = checkOpen.Checked,
                    Rating = Convert.ToInt32(editRating.Value)
                };
            }
            else if (rdioBar.Checked)
            {
                return new Bar()
                {
                    Id = Convert.ToInt32(edtId.Value),
                    Name = edtName.Text,
                    Adress = edtAdress.Text,
                    isOpened = checkOpen.Checked,
                    Rating = Convert.ToInt32(editRating.Value)
                };
            }
            return null;
        }

        private void brnSave_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = null;
                Presenter.SaveInstitution();
            }
            catch (Exception exception)
            {
                lblError.Text = exception.Message;
            }       
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            lblError.Text = null;
            Presenter.CancelAddingInstitution();
        }
    }
}
