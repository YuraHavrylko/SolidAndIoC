﻿namespace SolidAndIoC.Views
{
    partial class InstitutionEditView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkOpen = new System.Windows.Forms.CheckBox();
            this.edtName = new System.Windows.Forms.TextBox();
            this.edtAdress = new System.Windows.Forms.TextBox();
            this.brnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.editRating = new System.Windows.Forms.NumericUpDown();
            this.edtId = new System.Windows.Forms.NumericUpDown();
            this.rdioRestaurant = new System.Windows.Forms.RadioButton();
            this.rdioBar = new System.Windows.Forms.RadioButton();
            this.frpBoxEntertaimentInstitution = new System.Windows.Forms.GroupBox();
            this.lblError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.editRating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtId)).BeginInit();
            this.frpBoxEntertaimentInstitution.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Adress";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Rating";
            // 
            // checkOpen
            // 
            this.checkOpen.AutoSize = true;
            this.checkOpen.Location = new System.Drawing.Point(138, 159);
            this.checkOpen.Name = "checkOpen";
            this.checkOpen.Size = new System.Drawing.Size(63, 17);
            this.checkOpen.TabIndex = 4;
            this.checkOpen.Text = "Is Open";
            this.checkOpen.UseVisualStyleBackColor = true;
            // 
            // edtName
            // 
            this.edtName.Location = new System.Drawing.Point(79, 76);
            this.edtName.Name = "edtName";
            this.edtName.Size = new System.Drawing.Size(102, 20);
            this.edtName.TabIndex = 6;
            // 
            // edtAdress
            // 
            this.edtAdress.Location = new System.Drawing.Point(79, 117);
            this.edtAdress.Name = "edtAdress";
            this.edtAdress.Size = new System.Drawing.Size(100, 20);
            this.edtAdress.TabIndex = 7;
            // 
            // brnSave
            // 
            this.brnSave.Location = new System.Drawing.Point(27, 202);
            this.brnSave.Name = "brnSave";
            this.brnSave.Size = new System.Drawing.Size(75, 23);
            this.brnSave.TabIndex = 9;
            this.brnSave.Text = "Save";
            this.brnSave.UseVisualStyleBackColor = true;
            this.brnSave.Click += new System.EventHandler(this.brnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(126, 202);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // editRating
            // 
            this.editRating.Location = new System.Drawing.Point(79, 156);
            this.editRating.Name = "editRating";
            this.editRating.Size = new System.Drawing.Size(37, 20);
            this.editRating.TabIndex = 11;
            // 
            // edtId
            // 
            this.edtId.Location = new System.Drawing.Point(36, 9);
            this.edtId.Name = "edtId";
            this.edtId.Size = new System.Drawing.Size(36, 20);
            this.edtId.TabIndex = 12;
            // 
            // rdioRestaurant
            // 
            this.rdioRestaurant.AutoSize = true;
            this.rdioRestaurant.Location = new System.Drawing.Point(11, 19);
            this.rdioRestaurant.Name = "rdioRestaurant";
            this.rdioRestaurant.Size = new System.Drawing.Size(77, 17);
            this.rdioRestaurant.TabIndex = 13;
            this.rdioRestaurant.TabStop = true;
            this.rdioRestaurant.Text = "Restaurant";
            this.rdioRestaurant.UseVisualStyleBackColor = true;
            // 
            // rdioBar
            // 
            this.rdioBar.AutoSize = true;
            this.rdioBar.Location = new System.Drawing.Point(11, 35);
            this.rdioBar.Name = "rdioBar";
            this.rdioBar.Size = new System.Drawing.Size(41, 17);
            this.rdioBar.TabIndex = 14;
            this.rdioBar.TabStop = true;
            this.rdioBar.Text = "Bar";
            this.rdioBar.UseVisualStyleBackColor = true;
            // 
            // frpBoxEntertaimentInstitution
            // 
            this.frpBoxEntertaimentInstitution.Controls.Add(this.rdioRestaurant);
            this.frpBoxEntertaimentInstitution.Controls.Add(this.rdioBar);
            this.frpBoxEntertaimentInstitution.Location = new System.Drawing.Point(79, 9);
            this.frpBoxEntertaimentInstitution.Name = "frpBoxEntertaimentInstitution";
            this.frpBoxEntertaimentInstitution.Size = new System.Drawing.Size(134, 60);
            this.frpBoxEntertaimentInstitution.TabIndex = 15;
            this.frpBoxEntertaimentInstitution.TabStop = false;
            this.frpBoxEntertaimentInstitution.Text = "Entertaiment Institutions";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(12, 234);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 16;
            // 
            // InstitutionEditView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 256);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.frpBoxEntertaimentInstitution);
            this.Controls.Add(this.edtId);
            this.Controls.Add(this.editRating);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.brnSave);
            this.Controls.Add(this.edtAdress);
            this.Controls.Add(this.edtName);
            this.Controls.Add(this.checkOpen);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InstitutionEditView";
            this.Text = "InstitutionEditView";
            ((System.ComponentModel.ISupportInitialize)(this.editRating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtId)).EndInit();
            this.frpBoxEntertaimentInstitution.ResumeLayout(false);
            this.frpBoxEntertaimentInstitution.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkOpen;
        private System.Windows.Forms.TextBox edtName;
        private System.Windows.Forms.TextBox edtAdress;
        private System.Windows.Forms.Button brnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown editRating;
        private System.Windows.Forms.NumericUpDown edtId;
        private System.Windows.Forms.RadioButton rdioRestaurant;
        private System.Windows.Forms.RadioButton rdioBar;
        private System.Windows.Forms.GroupBox frpBoxEntertaimentInstitution;
        private System.Windows.Forms.Label lblError;
    }
}