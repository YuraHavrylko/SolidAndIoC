﻿namespace SolidAndIoC.Views
{
    partial class InstitutionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridDataInstitution = new System.Windows.Forms.DataGridView();
            this.btmAdd = new System.Windows.Forms.Button();
            this.btmEdit = new System.Windows.Forms.Button();
            this.btmRemove = new System.Windows.Forms.Button();
            this.btmOpenClose = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btmAddRandom = new System.Windows.Forms.Button();
            this.btmRecalcRating = new System.Windows.Forms.Button();
            this.lblErrorRating = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataInstitution)).BeginInit();
            this.SuspendLayout();
            // 
            // gridDataInstitution
            // 
            this.gridDataInstitution.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDataInstitution.Location = new System.Drawing.Point(12, 47);
            this.gridDataInstitution.Name = "gridDataInstitution";
            this.gridDataInstitution.Size = new System.Drawing.Size(544, 206);
            this.gridDataInstitution.TabIndex = 0;
            // 
            // btmAdd
            // 
            this.btmAdd.Location = new System.Drawing.Point(13, 13);
            this.btmAdd.Name = "btmAdd";
            this.btmAdd.Size = new System.Drawing.Size(75, 23);
            this.btmAdd.TabIndex = 1;
            this.btmAdd.Text = "Add";
            this.btmAdd.UseVisualStyleBackColor = true;
            this.btmAdd.Click += new System.EventHandler(this.btmAdd_Click);
            // 
            // btmEdit
            // 
            this.btmEdit.Location = new System.Drawing.Point(104, 12);
            this.btmEdit.Name = "btmEdit";
            this.btmEdit.Size = new System.Drawing.Size(75, 23);
            this.btmEdit.TabIndex = 2;
            this.btmEdit.Text = "Edit";
            this.btmEdit.UseVisualStyleBackColor = true;
            this.btmEdit.Click += new System.EventHandler(this.btmEdit_Click);
            // 
            // btmRemove
            // 
            this.btmRemove.Location = new System.Drawing.Point(195, 13);
            this.btmRemove.Name = "btmRemove";
            this.btmRemove.Size = new System.Drawing.Size(75, 23);
            this.btmRemove.TabIndex = 3;
            this.btmRemove.Text = "Remove";
            this.btmRemove.UseVisualStyleBackColor = true;
            this.btmRemove.Click += new System.EventHandler(this.btmRemove_Click);
            // 
            // btmOpenClose
            // 
            this.btmOpenClose.Location = new System.Drawing.Point(481, 13);
            this.btmOpenClose.Name = "btmOpenClose";
            this.btmOpenClose.Size = new System.Drawing.Size(75, 23);
            this.btmOpenClose.TabIndex = 4;
            this.btmOpenClose.Text = "Open/Close";
            this.btmOpenClose.UseVisualStyleBackColor = true;
            this.btmOpenClose.Click += new System.EventHandler(this.btmOpenClose_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 273);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Refresh Data";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btmAddRandom
            // 
            this.btmAddRandom.Location = new System.Drawing.Point(104, 273);
            this.btmAddRandom.Name = "btmAddRandom";
            this.btmAddRandom.Size = new System.Drawing.Size(85, 23);
            this.btmAddRandom.TabIndex = 6;
            this.btmAddRandom.Text = "Add Random";
            this.btmAddRandom.UseVisualStyleBackColor = true;
            this.btmAddRandom.Click += new System.EventHandler(this.btmAddRandom_Click);
            // 
            // btmRecalcRating
            // 
            this.btmRecalcRating.Location = new System.Drawing.Point(214, 273);
            this.btmRecalcRating.Name = "btmRecalcRating";
            this.btmRecalcRating.Size = new System.Drawing.Size(117, 23);
            this.btmRecalcRating.TabIndex = 7;
            this.btmRecalcRating.Text = "Recalculate Rating";
            this.btmRecalcRating.UseVisualStyleBackColor = true;
            this.btmRecalcRating.Click += new System.EventHandler(this.btmRecalcRating_Click);
            // 
            // lblErrorRating
            // 
            this.lblErrorRating.AutoSize = true;
            this.lblErrorRating.Location = new System.Drawing.Point(211, 313);
            this.lblErrorRating.Name = "lblErrorRating";
            this.lblErrorRating.Size = new System.Drawing.Size(0, 13);
            this.lblErrorRating.TabIndex = 8;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Location = new System.Drawing.Point(277, 22);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(0, 13);
            this.lblError.TabIndex = 9;
            // 
            // InstitutionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 335);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblErrorRating);
            this.Controls.Add(this.btmRecalcRating);
            this.Controls.Add(this.btmAddRandom);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btmOpenClose);
            this.Controls.Add(this.btmRemove);
            this.Controls.Add(this.btmEdit);
            this.Controls.Add(this.btmAdd);
            this.Controls.Add(this.gridDataInstitution);
            this.Name = "InstitutionView";
            this.Text = "InstitutionView";
            ((System.ComponentModel.ISupportInitialize)(this.gridDataInstitution)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView gridDataInstitution;
        private System.Windows.Forms.Button btmAdd;
        private System.Windows.Forms.Button btmEdit;
        private System.Windows.Forms.Button btmRemove;
        private System.Windows.Forms.Button btmOpenClose;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btmAddRandom;
        private System.Windows.Forms.Button btmRecalcRating;
        private System.Windows.Forms.Label lblErrorRating;
        private System.Windows.Forms.Label lblError;
    }
}