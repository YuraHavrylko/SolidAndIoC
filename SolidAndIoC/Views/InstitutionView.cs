﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SolidAndIoC.Contracts.Presenters;
using SolidAndIoC.Contracts.Views;

namespace SolidAndIoC.Views
{
    public partial class InstitutionView : Form, IInstitutionView
    {
        public InstitutionView()
        {
            InitializeComponent();
        }

        public IInstitutionPresenter Presenter { get; set; }

        public void ShowView()
        {
            Show();
        }

        public void CloseView()
        {
            Close();
        }

        public void Refresh(IEnumerable<EntertainmentInstitution> institutions)
        {
            gridDataInstitution.DataSource = institutions;
            gridDataInstitution.Refresh();
        }

        public EntertainmentInstitution GetSelectedInstitution()
        {
            if (gridDataInstitution.SelectedRows.Count < 1)
                return null;
            return gridDataInstitution.SelectedRows[0].DataBoundItem as EntertainmentInstitution;
        }

        private void btmAdd_Click(object sender, EventArgs e)
        {
            Presenter.AddInstitution();
        }

        private void btmEdit_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = null;
                Presenter.EditInstitution();
            }
            catch (Exception)
            {
                lblError.Text = "Select object!";
            }
            
        }

        private void btmRemove_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = null;
                Presenter.RemoveInstitution();
            }
            catch (Exception)
            {
                lblError.Text = "Select object!";
            }
            
        }

        private void btmAddRandom_Click(object sender, EventArgs e)
        {    
            try
            {
                lblError.Text = null;
                Presenter.AddRandom();
            }
            catch (Exception)
            {
                lblError.Text = "No items listed!";
            }
        }

        private void btmRecalcRating_Click(object sender, EventArgs e)
        {
            try
            {
                lblErrorRating.Text = null;
                Presenter.RecalculateRating();
            }
            catch (InvalidOperationException)
            {
                lblError.Text = "No items listed!";
            }
            catch (Exception exception)
            {
                lblErrorRating.Text = exception.Message;
            }

        }

        private void btmOpenClose_Click(object sender, EventArgs e)
        {  
            try
            {
                lblError.Text = null;
                Presenter.ChangeOpen();
            }
            catch (Exception)
            {
                lblError.Text = "Select object!";
            }
        }
    }
}
